import java.util.*;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node(String n, Node d, Node r) {
        this.name = n;
        this.firstChild = d;
        this.nextSibling = r;
    }

    public static Node parsePostfix(String s) {
        // Checking for input errors
        if (s.contains("\t")) {
            throw new RuntimeException("Tabs are not allowed " + s);
        }else if (s.contains("()")) {
            throw new RuntimeException("The subtree is empty " + s);
        } else if (s.contains(" ")) {
            throw new RuntimeException("No spaces allowed " + s);
        } else if (s.contains(",,")) {
            throw new RuntimeException("String can't contain of two commas following eachother " + s);
        } else if (s.contains("(,")) {
            throw new RuntimeException("Opening parenthesis can't be followed by a comma " + s);
        } else if (s.contains("))")) {
            throw new RuntimeException("Can't contain double closing parenthesis " + s);
        }

        Stack<Node> stack = new Stack<>();
        Node parsedNode = new Node(null, null, null);
        StringTokenizer st = new StringTokenizer(s, "(),", true);
        while (st.hasMoreTokens()) {
            String element = st.nextToken().trim();
            if (element.equals("(")) {
                stack.push(parsedNode);
                parsedNode.firstChild = new Node(null, null, null);
                parsedNode = parsedNode.firstChild;
            } else if (element.equals(")")) {
                Node tmp = stack.pop();
                parsedNode = tmp;
            } else if (element.equals(",")) {
                if (stack.empty()) {
                    throw new RuntimeException("Comma can't be alone" + s);
                }
                parsedNode.nextSibling = new Node(null, null, null);
                parsedNode = parsedNode.nextSibling;
            } else {
                parsedNode.name = element;
            }
        }
        return parsedNode;
    }

    public String leftParentheticRepresentation() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.name);
        if (this.firstChild != null) {
            sb.append("(");
            sb.append(this.firstChild.leftParentheticRepresentation());
            sb.append(")");
        }

        if (this.nextSibling != null) {
            sb.append(",");
            sb.append(this.nextSibling.leftParentheticRepresentation());
        }
        return sb.toString();
    }

    public static void main(String[] param) {
        String s = "(B1,C)A";
        Node t = Node.parsePostfix(s);
        String v = t.leftParentheticRepresentation();
        System.out.println(s + " ==> " + v); // (B1,C)A ==> A(B1,C)
    }
}

